import {DefaultCrudRepository} from '@loopback/repository';
import {Log, LogRelations} from '../models';
import {WeatherDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class LogRepository extends DefaultCrudRepository<
  Log,
  typeof Log.prototype._id,
  LogRelations
> {
  constructor(
    @inject('datasources.weather') dataSource: WeatherDataSource,
  ) {
    super(Log, dataSource);
  }
}
